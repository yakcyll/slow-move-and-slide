extends Node3D


# Called when the node enters the scene tree for the first time.
func _ready():
	var ball = get_node("BallBody")
	ball.init()


func _physics_process(delta):
	var ball = get_node("BallBody")
	ball.update(delta)
