extends Node3D
class_name MeshInterpolator

var _current_transform: Transform3D
var _previous_transform: Transform3D
var _target: Node3D


func _ready():
	set_as_top_level(true)
	_target = get_parent()
	set_visible(true)
	set_process_priority(100)


func set_visible(new_visible: bool) -> void:
	visible = new_visible
	set_process(visible)
	set_physics_process(visible)


func _process(_delta):
	var weight = Engine.get_physics_interpolation_fraction()

	# Transpose.
	var position_delta = _current_transform.origin - _previous_transform.origin
	global_transform.origin = _previous_transform.origin + position_delta * weight

	# Rotate.
	global_transform.basis = _previous_transform.basis.slerp(_current_transform.basis, weight)


func update(_delta: float) -> void:
	_previous_transform = _current_transform
	_current_transform = _target.global_transform
