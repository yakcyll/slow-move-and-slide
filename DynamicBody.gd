extends CharacterBody3D
class_name DynamicBody

var _direction = Vector3.ZERO
var _velocity = Vector3.ZERO
var _gravity_strength = 1.0
var _gravity_direction = Vector3.DOWN
var _bounce_factor = 0.0
var _slide_damp = 0.1


func _ready() -> void:
	set_safe_margin(0.01)
	set_floor_constant_speed_enabled(true)


func set_bounce_factor(bounce_factor: float) -> void:
	_bounce_factor = bounce_factor


func set_slide_damp(slide_damp: float) -> void:
	_slide_damp = slide_damp


func set_internal_velocity(velocity: Vector3):
	print("wtf " + str(velocity))
	_velocity = velocity


func update(delta: float) -> void:
	var gravity_magnitude = 1.0

	var normal = Vector3.ZERO
	if is_on_floor():
		print("floor")
		normal = get_floor_normal()
	elif is_on_ceiling() or is_on_wall():
		print("ceiling or wall")
		if get_slide_collision_count() > 0:
			normal = get_slide_collision(0).get_normal()
	
	if normal != Vector3.ZERO:
		var reflected_velocity = _velocity.bounce(normal)

		# Reflect.
		if normal.dot(reflected_velocity) > 0.03:
			var next_velocity = reflected_velocity * _bounce_factor
			print(">>> something's wrong")
			print(_velocity)
			print(next_velocity)
			print(normal)
			print(reflected_velocity)
			print(normal.dot(reflected_velocity))
			set_velocity(next_velocity)
			set_up_direction(Vector3.UP)
			var a = Time.get_ticks_usec()
			move_and_slide()
			var b = Time.get_ticks_usec()
			print(velocity)
			print("move and slide under reflect takes: " + str(b-a) + "µs.")
			print("number of collisions: " + str(get_slide_collision_count()))
			_velocity = get_velocity()

		# Slide.
		else:
			print("wait are we here too")
			var gravity_delta = _gravity_direction * _gravity_strength * gravity_magnitude * delta

			var next_velocity = _velocity + gravity_delta
			var damped_velocity = next_velocity * (1.0 - _slide_damp * delta)

			# If after damping we get velocity in opposite direction, stop.
			if next_velocity.length() < 0.005:
				next_velocity = Vector3.ZERO

			# We make the normal shorter so that, if for some reason we are
			# supposed to be launched away from the surface, it will happen.
			set_velocity(next_velocity)
			# set_floor_snap_length(0.2)
			# set_up_direction(Vector3.UP)
			# set_floor_stop_on_slope_enabled(false)
			var a = Time.get_ticks_usec()
			move_and_slide()
			var b = Time.get_ticks_usec()
			print("move and slide under slide takes: " + str(b-a) + "µs.")
			print("number of collisions: " + str(get_slide_collision_count()))
			_velocity = get_velocity()

			# GODOT-4: Pruning sliding in order to cut lag spikes
			if _velocity == next_velocity:
				next_velocity = Vector3.ZERO
				set_velocity(Vector3.ZERO)

	else:
		print("we here")
		_velocity += _gravity_direction * _gravity_strength * gravity_magnitude * 0.5 * delta
		set_velocity(_velocity)
		# set_up_direction(Vector3.UP)
		var a = Time.get_ticks_usec()
		move_and_slide()
		var b = Time.get_ticks_usec()
		print("move and slide under air takes: " + str(b-a) + "µs.")
		print("number of collisions: " + str(get_slide_collision_count()))


func _on_gravity_changed(_name: String, value: float) -> void:
	_gravity_strength = value
