extends DynamicBody
class_name BallBody


func init():

	_create_ball_body()
	set_bounce_factor(0.6)
	set_slide_damp(0.2)


func get_mesh_container() -> Node3D:
	return get_node("BallMeshInterpolator/BallMesh") as Node3D


func get_mesh() -> MeshInstance3D:
	return get_mesh_container().get_child(0) as MeshInstance3D


func _create_ball_body():
	var mesh_interpolator = _create_ball_mesh_interpolator()
	var mesh_container = _create_ball_mesh()
	var collision_shape = _create_ball_collision(mesh_container)

	mesh_interpolator.add_child(mesh_container)
	add_child(mesh_interpolator)
	add_child(collision_shape)


func _create_ball_mesh_interpolator() -> MeshInterpolator:
	var interpolator = MeshInterpolator.new()
	interpolator.name = "BallMeshInterpolator"
	return interpolator


func _create_ball_mesh() -> Node3D:
	var mesh_container = Node3D.new()
	var ball_mesh = MeshInstance3D.new();
	ball_mesh.mesh = SphereMesh.new();
	ball_mesh.mesh.radius = 0.1;
	ball_mesh.name = "ball_mesh"
	mesh_container.add_child(ball_mesh)
	mesh_container.name = "BallMeshContainer"

	return mesh_container


func _create_ball_collision(mesh_container: Node3D) -> CollisionShape3D:
	var main_mesh = mesh_container.get_child(0)
	main_mesh.create_convex_collision()

	var _static_body = main_mesh.get_node(String(main_mesh.name) + "_col")
	var collision_shape: CollisionShape3D = _static_body.get_child(0)
	collision_shape.name = "BallCollision"
	collision_shape.shape.margin = 0.1

	collision_shape.transform.basis = Basis.from_euler(Vector3(deg_to_rad(90.0), deg_to_rad(0.0), deg_to_rad(0.0))).scaled(
		mesh_container.scale
	)

	_static_body.remove_child(collision_shape)
	main_mesh.remove_child(_static_body)

	return collision_shape
