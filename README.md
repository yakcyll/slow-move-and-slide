# Slow move_and_slide in Godot 4 (a8)

An excerpt of physics code for a dynamic spherical body that takes a very long time (in tens of milliseconds) to complete a call to move_and_slide upon colliding with another (static) body. Tested in 3.5, where it works without hitches, and 4.0a3 and 4.0a8, where said calls take even up to 80ms to complete on i5-1235U with an iGPU.
